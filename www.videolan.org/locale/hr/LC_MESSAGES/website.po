# Croatian translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Dominko Aždajić <domazd@mail.ru>, 2013
# gogo <trebelnik2@gmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2017-10-06 14:28+0200\n"
"Last-Translator: VideoLAN <videolan@videolan.org>, 2017\n"
"Language-Team: Croatian (http://www.transifex.com/yaron/vlc-trans/language/"
"hr/)\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: include/header.php:289
msgid "a project and a"
msgstr ""

#: include/header.php:289
msgid "non-profit organization"
msgstr "neprofitna organizacija"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Partneri"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr ""

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr ""

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Događaji"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Pravna pitanja"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Tiskovni centar"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Kontaktirajte nas"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Preuzimanje"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Značajke"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr ""

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Nabavite poslastice"

#: include/menus.php:51
msgid "Projects"
msgstr "Projekti"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Svi projekti"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Suradnja"

#: include/menus.php:77
msgid "Getting started"
msgstr ""

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr ""

#: include/menus.php:79
msgid "Report a bug"
msgstr "Prijavite grešku"

#: include/menus.php:83
msgid "Support"
msgstr "Podrška"

#: include/footer.php:33
msgid "Skins"
msgstr "Presvlake"

#: include/footer.php:34
msgid "Extensions"
msgstr "Proširenja"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Zaslonske snimke"

#: include/footer.php:61
msgid "Community"
msgstr "Zajednica"

#: include/footer.php:64
msgid "Forums"
msgstr "Forum"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Mailing lista"

#: include/footer.php:66
msgid "FAQ"
msgstr "Često postavljana pitanja"

#: include/footer.php:67
msgid "Donate money"
msgstr "Donirajte novac"

#: include/footer.php:68
msgid "Donate time"
msgstr "Posvetite vrijeme"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Projekt i organizacija"

#: include/footer.php:76
msgid "Team"
msgstr "Tim"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Zrcalni poslužitelji"

#: include/footer.php:83
msgid "Security center"
msgstr "Centar sigurnosti"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Uključite se"

#: include/footer.php:85
msgid "News"
msgstr "Novosti"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Preuzmite VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Ostali sustavi"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr ""

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC je slobodan i besplatan multimedijski izvođač otvorenog kôda za razne "
"operacijske sustave, kao i okvir za izvođenje većine multimedijskih "
"datoteka, audio i video diskova te raznih protokola za strujanje audio i "
"video sadržaja."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr ""
"VLC: Službene stranice - Slobodna multimedijalna rješenja za sve operacijske "
"sustave!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Drugi VideoLAN projekti"

#: index.php:30
msgid "For Everyone"
msgstr "Za sve korisnike"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC je moćan multimedijski izvođač, koji podržava većinu medijskih kodeka i "
"video formata."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN Movie Creator je nelinearni softver za stvaranje i uređivanje video "
"snimaka."

#: index.php:62
msgid "For Professionals"
msgstr "Za profesionalne korisnike"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast je jednostavna i moćna aplikacija za MPEG-2/TS demultipleksiranje i "
"strujanje."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat je skup alata za lakše i učinkovitiје upravljanje višesmjernim i "
"transportnim strujanjima (TS)."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 je besplatna aplikacija za enkôdiranje video zapisa u  H.264/MPEG-4 AVC "
"format."

#: index.php:104
msgid "For Developers"
msgstr "Za razvijatelje"

#: index.php:140
msgid "View All Projects"
msgstr "Prikaži sve projekte"

#: index.php:144
msgid "Help us out!"
msgstr "Pomozite nam!"

#: index.php:148
msgid "donate"
msgstr "donirajte"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN je neprofitna organizacija."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Sve naše troškove pokrivaju donacije, koje dobivamo od naših korisnika. "
"Ukoliko Vam je koristan neki VideoLAN proizvod, donirajte da nas podržite."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Saznajte više"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN je softver otvorenog kôda."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"To znači da je Vaša suradnja dobrodošla, ukoliko imate sposobnost i želju da "
"poboljšate neki od naših proizvoda"

#: index.php:187
msgid "Spread the Word"
msgstr "Preporučite dalje"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Držimo da VideoLAN nudi najbolji video softver po najboljoj cijeni: "
"besplatno. Ukoliko se slažete s time, pomozite nam da naš software učinimo "
"još poznatijim."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Novosti i nadopune"

#: index.php:218
msgid "More News"
msgstr ""

#: index.php:222
msgid "Development Blogs"
msgstr "Blogovi o razvoju"

#: index.php:251
msgid "Social media"
msgstr "Društvene mreže"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "VLC za"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr ""

#: vlc/index.php:35
msgid "Plays everything"
msgstr ""

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr ""

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr ""

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr ""

#: vlc/index.php:44
msgid "Completely Free"
msgstr ""

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr ""

#: vlc/index.php:47
msgid "learn more"
msgstr ""

#: vlc/index.php:66
msgid "Add"
msgstr ""

#: vlc/index.php:66
msgid "skins"
msgstr ""

#: vlc/index.php:69
msgid "Create skins with"
msgstr ""

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr ""

#: vlc/index.php:72
msgid "Install"
msgstr ""

#: vlc/index.php:72
msgid "extensions"
msgstr ""

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Prikaži sve snimke zaslona"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Službena preuzimanja VLC izvođača medija"

#: vlc/index.php:146
msgid "Sources"
msgstr "Izvorni kôd"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Izravno možete preuzeti i"

#: vlc/index.php:148
msgid "source code"
msgstr "izvorni kôd"

#~ msgid "A project and a"
#~ msgstr "Projekt i"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "sastavljena od dobrovoljaca, koji razvijaju i promiču slobodna "
#~ "multimedijalna rješenja otvorenog kôda."

#~ msgid "why?"
#~ msgstr "zašto?"

#~ msgid "Home"
#~ msgstr "Naslovnica"

#~ msgid "Support center"
#~ msgstr "Centar podrške"

#~ msgid "Dev' Zone"
#~ msgstr "Zona razvijatelja"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Jednostavan, brz i moćan izvođač medija."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "Izvodi sve: datoteke, diskove, web kamere, uređaje i strujanja."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "Za izvođenje većine kôdeka nisu potrebni paketi kôdeka:"

#~ msgid "Runs on all platforms:"
#~ msgstr "Radi na svim platformama:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr ""
#~ "U potpunosti besplatan, bez zlonamjernog softvera, reklama i praćenja "
#~ "korisnika."

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Može strujati i pretvarati medije."

#~ msgid "Discover all features"
#~ msgstr "Otkrijte sve značajke"

#~ msgid "DONATE"
#~ msgstr "DARUJTE PRILOG"

#~ msgid "Other Systems and Versions"
#~ msgstr "Drugi sustavi i inačice"

#~ msgid "Other OS"
#~ msgstr "Drugi operacijski sustavi"
